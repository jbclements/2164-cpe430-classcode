#lang typed/racket

(require typed/rackunit)

(define-type Sto Natural)
(define empty-sto 1234)

;; an ExprC represents an expression to be evaluated:
(define-type ExprC (U NumC IdC If0C AppC LamC RecC))
(struct NumC ([n : Real]) #:transparent)
(struct IdC ([x : Symbol]) #:transparent)
(struct AppC ([fun : ExprC] [args : (Listof ExprC)]) #:transparent)
(struct LamC ([param : Symbol] [body : ExprC]) #:transparent)
(struct If0C ([test : ExprC] [then : ExprC] [else : ExprC])
  #:transparent)
(struct RecC ([var : Symbol] [rhs : ExprC] [body : ExprC])
  #:transparent)

;; a Value represents the result of evaluation:
(define-type Value (U NumV CloV PrimV NullV))
(struct NumV ([n : Real]) #:transparent)
(struct CloV ([param : Symbol] [body : ExprC] [env : Env]) #:transparent)
(define-type PrimV (U '+Prim '*Prim '=Prim))
(define-type NullV 'null)

;; an Environment is a hash table mapping symbols to values
(define-type Env (HashTable Symbol (Boxof Value)))
(: top-env Env)
(define top-env (hash '+ ((inst box Value) '+Prim)
                      '* ((inst box Value) '*Prim)))

;; parse an s-expression into an ExprC
(: parse (Sexp -> ExprC))
(define (parse sexp)
  (match sexp
    [(? real? n) (NumC n)]
    [(? symbol? x) (IdC x)]
    [(list 'lam (list (? symbol? param)) body)
     (LamC param (parse body))]
    [(list 'rec (list (? symbol? var) '= rhs) body)
     (RecC var (parse rhs) (parse body))]
    [(list 'if0 test then else)
     (If0C (parse test) (parse then) (parse else))]
    [(list fun arg ...)
     (AppC (parse fun) (map parse (cast arg (Listof Sexp))))]
    [other
     (error 'parse
            "expected legal expression, got ~e"
            sexp)]))

;; a few test cases for parse
(check-equal? (parse '3) (NumC 3))
(check-equal? (parse '{+ 3 4})
              (AppC (IdC '+) (list (NumC 3) (NumC 4))))
(check-equal? (parse '{* {+ 2 2} 13})
              (AppC (IdC '*) (list
                              (AppC (IdC '+) (list (NumC 2) (NumC 2)))
                              (NumC 13))))
(check-equal? (parse '{g {+ 13 x}})
              (AppC (IdC 'g) (list
                              (AppC (IdC '+) (list (NumC 13) (IdC 'x))))))
(check-equal? (parse '{unbox {box 13}})
              (AppC (IdC 'unbox) (list (AppC (IdC 'box)
                                             (list (NumC 13))))))

;; interpret an expression in a given environment
(: interp (ExprC Env -> Value))
(define (interp expr env)
  (match expr
    [(NumC n) (NumV n)]
    [(LamC param body) (CloV param body env)]
    [(IdC x) (unbox (hash-ref env x))]
    [(If0C test then else)
     (if (equal? (interp test env) (NumV 0))
         (interp then env)
         (interp else env))]
    [(AppC fun args)
     (: fval Value)
     (define fval (interp fun env))
     (: argvals (Listof Value))
     (define argvals (map (λ ([a : ExprC]) (interp a env)) args))
     (match fval
       [(CloV param body clo-env)
        (when (not (= (length args) 1))
          (error "non-prim app limited to exactly one argument"))
        (interp body
                (hash-set clo-env param (box (first argvals))))]
       ['null
        (error 'interp
               "can't apply null as function in expr: ~e"
               expr)]
       [prim
        ((hash-ref prim-defs prim) argvals)])]
    [(RecC var rhs body)
     (define newbox ((inst box Value) 'null))
     (define newenv (hash-set env var newbox))
     (define rhsval (interp rhs newenv))
     (set-box! newbox rhsval)
     (interp body newenv)]))

(define-type PrimImpl ((Listof Value) -> Value))

;; implementation of plus operator
(: myplus PrimImpl)
(define (myplus args)
  (match args
    [(list (NumV n1) (NumV n2))
     (NumV (+ n1 n2))]))

;; implementation of times operator
(: mytimes PrimImpl)
(define (mytimes args)
  (match args
    [(list (NumV n1) (NumV n2))
     (NumV (* n1 n2))]))

(: prim-defs (HashTable Value PrimImpl))
(define prim-defs
  (make-immutable-hash
   (list (cons '+Prim myplus)
         (cons '*Prim mytimes))))

;; a few test cases for interp:
(check-equal? (interp (parse '{+ 3 4}) top-env)
              (NumV 7))
(check-equal? (interp (parse '{* 3 {+ 4 5}}) top-env)
              (NumV 27))

(check-equal? (interp (parse '{v {+ 3 4}})
                      (hash-set
                       (hash-set
                        top-env
                        'z
                        ((inst box Value) (CloV 'q (parse '{+ q 1}) top-env)))
                       'v
                       ((inst box Value) (CloV 'q (parse '{+ q -1}) top-env))))
              (NumV 6))

(check-exn #px"no value found for key"
           (λ ()
             (interp (parse '{calls-b 3})
                     (hash
                      'calls-b ((inst box Value)
                                (CloV 'x (parse '{b 24}) top-env))
                      'b       ((inst box Value)
                                (CloV 'y (parse '{+ x y}) top-env))))))

(check-equal?
 (interp (parse '{rec {fact = {lam {x}
                                   {if0 x
                                        1
                                        {* x {fact {+ x -1}}}}}}
                   {fact 4}})
         top-env)
 (NumV 24))











